
package server;

import java.util.Iterator;
import javax.vecmath.Vector3d;

public class Alarm extends Bullet {

    public Alarm(Vector3d loc, Vector3d delta, Ship owner) {
        super(loc, delta, owner);
    }
    
    public void move() {
        // do nothing
    }
    
    public boolean matches(Vector3d check) {
        
        return false;
    }
    
    public void collision(Ship other) {
        if(other != owner) {
            Iterator<Coords> iter = other.path.descendingIterator();
            while(iter.hasNext()) {
                StringBuilder str = new StringBuilder();
                str.append("ALARM:");
                str.append(other.playerNum+"");
                for(int count = 0; count < 10 && iter.hasNext(); count++) {
                    str.append(":");
                    str.append(Ship.locToString(iter.next()));
                }
                owner.sendMsg("", str.toString());
            }
            Server.s.myWorld.bullets.remove(this);
        }
    }
    
}
