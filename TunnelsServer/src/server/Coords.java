
package server;

import javax.vecmath.Vector3d;

public class Coords extends Vector3d {
    
    public static final int roundTo = 2;

    Coords(double[] generateCoord) {
        super(generateCoord);
    }

    Coords(double a, double b, double c) {
        super(a, b, c);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(o instanceof Vector3d) {
            long ax, ay, az, bx, by, bz;
            Vector3d b = (Vector3d) o;
            ax =  Math.round(x / roundTo);
            ay =  Math.round(y / roundTo);
            az =  Math.round(z / roundTo);
            bx =  Math.round(b.x / roundTo);
            by =  Math.round(b.y / roundTo);
            bz =  Math.round(b.z / roundTo);
            if (ax == bx && ay == by && az == bz) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        long ax, ay, az;
        ax =  Math.round(x / roundTo);
        ay =  Math.round(y / roundTo);
        az =  Math.round(z / roundTo);
        return (int) (ax + ay + az);
    }
    
    
    
}
