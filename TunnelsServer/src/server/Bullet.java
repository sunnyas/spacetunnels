
package server;

import javax.vecmath.Vector3d;


public class Bullet {
    
    Vector3d loc, delta;
    Ship owner;
    int time;

    public Bullet(Vector3d loc, Vector3d delta, Ship owner) {
        this.loc = loc;
        if (delta != null)
        	delta.scale(.5);
        this.delta = delta;
        this.owner = owner;
        time = 0;
    }

    public void move() {
        loc.add(delta);
        time++;
        if(time > 33) {
            Server.s.myWorld.bullets.remove(this);
        }
    }
    
    public void collision(Ship other) {
        if(other != owner) {
            Server.s.myWorld.ships.remove(other);
            other.isAlive = false;
            other.sendMsg("DEATH", "");
            Server.s.myWorld.broadcastPlayers();
            Server.s.myWorld.bullets.remove(this);
        } 
    }    
}
