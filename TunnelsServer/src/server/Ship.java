package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedDeque;
import javax.vecmath.Vector3d;

public class Ship implements Runnable {

    private String userName;
    public Server myServer;
    private Socket mySocket;
    private BufferedReader in;
    PrintWriter out;
    Coords loc;
    ConcurrentLinkedDeque<Coords> path;
    private LinkedList<Alarm> alarms;
    // not size of path kept, but overall path since time 0
    long pathSize = 0;
    volatile boolean isAlive = true;
    int playerNum;
    
    static int playerCount = 0;
    static final int pathLength = 1000;
    static Random r = new Random();

    public static double[] generateCoord(int gridSize) {
        double[] a = new double[3];
        a[0] = r.nextDouble() * gridSize - gridSize / 2.0;
        a[1] = r.nextDouble() * gridSize - gridSize / 2.0;
        a[2] = r.nextDouble() * gridSize - gridSize / 2.0;
        return a;
    }

    public static String locToString(Vector3d loc) {
        return (loc.x + "," + loc.y + "," + loc.z);
    }
    
    public static Coords stringToLoc(String s) {
        String[] split = s.split(",");
        double a = Double.parseDouble(split[0]);
        double b = Double.parseDouble(split[1]);
        double c = Double.parseDouble(split[2]);
        return new Coords(a,b,c);
    }

    public Ship(Socket s, Server server) {
        playerNum = playerCount;
        userName = "Player " + (playerCount);
        playerCount += 1;
        mySocket = s;
        myServer = server;
        loc = new Coords(generateCoord(server.myWorld.getGridSize()));
        path = new ConcurrentLinkedDeque<>();
        alarms = new LinkedList<>();
        try {
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream(), true);
        } catch (java.io.IOException ioe) {
            server.print("Error: " + ioe.getMessage());
            //ioe.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean crash = false;
        try {
            while (mySocket.isConnected()) {
                String line = in.readLine();
                System.out.println(userName + ":" + line);
                if (line == null) {
                    commands("DISCONNECT");
                    break;
                } else {
                    commands(line);
                }
            }
        } catch (IOException e) {
            myServer.print("Client: " + userName + " disconnected.");
            System.out.println("Error:" + e.getMessage());
            crash = true;
        }

    }

    public void commands(String line) {
        for (String commandSplit : line.split("\n")) {
            if(commandSplit.isEmpty()) continue;
            String[] argSplit = commandSplit.split(":");
            Coords a, b;
            switch (argSplit[0]) {
                case "CONNECT":
                    sendMsg("LOCATION:", locToString(loc));
                    myServer.myWorld.broadcastPlayers();
                    break;
                case "DISCONNECT":
                    try {
                        myServer.myWorld.ships.remove(this);
                        myServer.myWorld.paths.values().removeAll(Collections.singleton(this));
                        mySocket.close();
                    } catch (IOException ex) {
                    }
                    break;
                case "LOCATION":
                    loc = stringToLoc(argSplit[1]);
                    path.add(loc);
                    Ship other = myServer.myWorld.paths.put(loc, this);
                    if(other != null && other != this) {
                        (new Alarm(loc, null, this)).collision(other);
                    }
                    if(pathSize > pathLength) {
                        a = path.poll();
                        myServer.myWorld.paths.remove(a);
                        for(Alarm al : alarms) {
                            if(al.loc.equals(a)) {
                                alarms.remove(al);
                                break;
                            }
                        }
                    }
                    pathSize++;
                    break;
                case "FIRE":
                    a = stringToLoc(argSplit[1]);
                    b = stringToLoc(argSplit[2]);
                    myServer.myWorld.bullets.add(new Bullet(a, b, this));
                    break;
                case "ALARM":
                    Alarm alarm = new Alarm(new Vector3d(stringToLoc(argSplit[1])),
                            null, this);
                    myServer.myWorld.bullets.add(alarm);
                    if(alarms.size() == 3) {
                        myServer.myWorld.bullets.remove(alarms.removeFirst());
                    }
                    alarms.addLast(alarm);
                    break;
                case "DEATH":
                    isAlive = false;
                    break;
                default:
                    System.out.println("SHIT: " + line);
                    break;
            }
            if(!isAlive) {
                for(Ship s: myServer.myWorld.ships) {
                    if(s == this) continue;
                    (new Alarm(loc, null, this)).collision(s);
                }
            }
        }
    }

    public void sendMsg(String command, String args) {
        StringBuilder str = new StringBuilder();
        str.append(command);
        str.append(args);
        str.append("\n");
        out.write(str.toString());
        out.flush();
    }
    
    
}
