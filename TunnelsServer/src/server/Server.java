
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server implements Runnable {
    
    static Server s;
    
    public int port = 13370;
    /** Main thread for Server */
    public Thread mainloop;
    public boolean online = false;
    ServerSocket serverSocket = null;
    World myWorld = new World();
    Socket clientSocket = null;
    Ship newPlayer = null;
    
    public void print(String s) {
        System.out.println(s);
    }
    
    
    public void startServer () {
        mainloop = new Thread(this);
        mainloop.start();
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            print("Server started, and listening on port " + port);
        } catch (IOException e) {
            print("Could not listen on port: " + port);
            return;
        }
        online = true;
        (new Thread(myWorld)).start();
        while (online) {
            try {
                clientSocket = serverSocket.accept();
                newPlayer = new Ship(clientSocket, this);
                (new Thread(newPlayer)).start();
                print("Client Accepted. Address: " + clientSocket.getInetAddress().toString().substring(1));
                print(clientSocket.toString());
            } catch (IOException e) {
                if (online) {
                    print("Accept failed, Error: " + e.getMessage());
                }
            }
        }
    }
    
    public static void main(String[] args) throws InterruptedException {
        s = new Server();
        s.startServer();
        
        // s.mainloop.join();
        while(s.clientSocket == null) {
            Thread.sleep(1000);
        }
        Scanner scan = new Scanner(System.in);
        int i = 0;
        while(true) {
            String str = scan.nextLine();
            s.newPlayer.out.println(str);
            s.newPlayer.out.println(i);
            i++;
        }
    }
    
}
