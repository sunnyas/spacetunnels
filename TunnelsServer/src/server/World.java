
package server;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.vecmath.Vector3d;


public class World implements Runnable{
    
    CopyOnWriteArrayList<Bullet> bullets;
    CopyOnWriteArrayList<Ship> ships;
    ConcurrentHashMap<Coords, Ship> paths;

    public World() {
        bullets = new CopyOnWriteArrayList<>();
        ships = new CopyOnWriteArrayList<>();
        paths = new ConcurrentHashMap<>();
    }
    
    public boolean collisionRange(Vector3d a, Vector3d b) {
        Vector3d diff = new Vector3d();
        diff.sub(a, b);
        if(diff.lengthSquared() < 9) {
            return true;
        }
        return false;
    }

    @Override
    public void run() {
        while(true) {
            for(Bullet b: bullets) {
                b.move();
                for(Ship s : ships) {
                    if(collisionRange(s.loc, b.loc)) {
                        b.collision(s);
                    }
                }
            }
            try {
                Thread.sleep(33);
            } catch (InterruptedException ex) {
                System.out.println("Bullet move thread died.");
                return;
            }
        }
    }
    
    public int getGridSize() {
        return ships.size()+1 * 25;
    }
    
    public void broadcastPlayers() {
        int size = ships.size();
        for(Ship s: ships) {
            s.sendMsg("NUMPLAYERS:", "" + size);
        }
    }
    
}
