#ifndef CYLINDER_H
#define CYLINDER_H

#define RADIUS 2;

#include "Vector3d.h"

class Cylinder
{
private:
	Vector3d pos, perp;
	double height, angle;
	float *color;
public:
	Cylinder(Vector3d point1, Vector3d point2, float col[3]);
	void draw();
};

#endif
