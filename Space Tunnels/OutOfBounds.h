#ifndef OUTOFBOUNDS_H
#define OUTOFBOUNDS_H


#include <list> // STL list
#include "Ship.h"
#include "World.h"
#include <iostream>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

class OutOfBounds
{
public:
	static void writeWords(std::string words, float posx, float posy);
	static bool checkCube(Ship* ship);
private:
	static bool showOutOfBoundsWarning();
	static void drawFace(Point3d p1, Point3d p2, float alpha);
};
#endif
