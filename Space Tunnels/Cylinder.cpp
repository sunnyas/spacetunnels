#include "Cylinder.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <math.h>

Cylinder::Cylinder(Vector3d point1, Vector3d point2, float col[3])
{
	pos = point1;
	Vector3d temp = point2 - point1;
	height = temp.length();
	perp = Vector3d::cross(temp, Vector3d::zUnit());
	angle = acos((point1[2]-point2[2])/height);
	color = col;
}

void Cylinder::draw()
{
	glPushMatrix();
		glColor3fv(color);
		glTranslatef(pos[0], pos[1], pos[2]);
		glRotated(angle*180/3.14, perp[0], perp[1], perp[2]);
		GLUquadric *quad = gluNewQuadric();
		gluCylinder(quad, 1, 1, height, 20, 20);
	glPopMatrix();
}