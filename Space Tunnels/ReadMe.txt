README

	Space Tunnels is a game inspired by the book Ender's Game by Orson Scott Card. The following is the passage from the book:

	"He was too small to see the controls, how the game was actually done. That didn�t matter. He got the movement of it in the air. The way the player dug tunnels in the darkness, tunnels of light, which the enemy ships would search for and then follow mercilessly until they caught the player�s ship. The player could make traps: mines, drifting bombs, loops in the air that forced the enemy ships to repeat endlessly. Some of the players were clever. Others lost quickly.
	Ender liked it better, though, when two boys played against each other. Then they had to use each other�s tunnels, and it quickly became clear which of them was worth anything at the strategy of it.
	Within an hour or so, it began to pall. Ender understood the regularities by then. Understood the rules the computer was following, so that he knew he could always, once he mastered the controls, outmaneuver the enemy. Spirals when the enemy was like this; loops when the enemy was like that. Lie in wait at one trap. Lay seven traps and then lure them like this. There was no challenge to it, then, just a matter of playing until the computer got so fast that no human reflexes could overcome it. That wasn�t fun. It was the other boys he wanted to play. The boys who had been so trained by the computer that even when they played against each other they each tried to emulate the computer. Think like a machine instead of a boy.
	I could beat them this way. I could beat them that way.
	[...]
	So Ender took his place at the unfamiliar controls. His hands were small, but the controls were simple enough. It took only a little experimentation to find out which buttons used certain weapons. Movement control was a standard wireball. His reflexes were slow at first. The other boy, whose name he still didn�t know, got ahead quickly. But Ender learned a lot and was doing much better by the time the game ended.
	[...]
	They played again, and this time Ender was deft enough to pull off a few maneuvers that the boy had obviously never seen before. His patterns couldn�t cope with them. Ender didn�t win easily, but he won.
	The bigger boys stopped laughing and joking then. The third game went in total silence. Ender won it quickly and efficiently."

CONTROLS
	WSAD/Arrow Keys for movement
	Spacebar to set alarm

AUTHORS
	Sid Asnani
		Networking (Server and Client)
	Joshua Berenhaus
		Documentation
		Out of bounds code
		Game environment
	Steven Southall
		Ship Movement
		Camera control

NETWORK COMMANDS
	CONNECT (client - tell server we connected)
	DISCONNECT (client - tell server we disconnected)
	DEATH (server - tell the player he died)
	LOCATION:X,Y,Z (server - give initial location of the client) (client - report location)
	FIRE:X,Y,Z:{DIRECTION_VECTOR represented by X,Y,Z} (client - fire weapon)
	ALARM:X,Y,Z (client - location of new alarm) ALARM:PLAYERNUM:X,Y,Z:X,Y,Z: ....... (server - send found enemy path to client)
	NUMPLAYERS:X (server - tell the client how many players there are)

	REMOVED:
	PATH:X,Y,Z:X,Y,Z: ....... (server - send found enemy path to client) (used ALARM instead.)

MAKING OF:
	We used OpenGL and C++ to create the client as well as the Boost library for networking. We created the server in java to coordinate the clients.

	Learning Opportunities:
		The Out Of Bounds code for the game actually draws the the text on a two dimensional HUD (Heads Up Display). This was especially challenging because we had to change the 3D frame and draw the 2D frame. It was a great learning experience because it increased our understanding of the stack the model view mode versus projection mode. If we had more time, the plan was to create a full HUD (Heads Up Display) with Alarms remaining, lives (if we decided to implement them), etc. which would be trivial with the code to change from the 3D world to a 2D HUD.
		The pathing in the game is drawn by storing the points in an array and drawing cylinders in openGL to represent the path. Since we obviously couldn't send an array object or a series of openGL cylinders, we decided to do it in a character array (or string). We learned that parsing a string can be very difficult when you don't have network commands and that it is very useful to include them in your README file so that all members of the team can use the same commands. We used these commands for connectivity, location, shooting, setting alarms, and pathing. The other thing we learned when we were making the networking code is that the boost library has amazing libraries for making network connections and sending information over the network. As you can see, most of our NetworkClient.cpp is calling functions out of the boost library to allow our client and server to talk to each other.
		The server was written in java out of an old IRC server that Sid wrote. We soon figured out that an IRC server and game server are not so different. They both accept messages from the clients and they both send messages back to the clients who then process that message (not so different from any type of server). We could then adapt the code to work for our game, although there were some differences that we had to adapt to along the way. An IRC server does not store any information and thus does not need to have a state. It just sends everyone in the IRC chat the message and waits for a new message. Our server was different because we had to have a state to store the locations of each player, the path and alarms of each player, and simulate shooting to see if we hit the opponent.
		Since a server runs multithreaded, we had to use some structures out of Java 7's java.util.concurrent library. CopyOnWriteArrayList, ConcurrentHashMap, and ConcurrentDeque were very useful for keeping track of the path, the ships, bullets, and collision with as fast as possible search time. We stored the path in the Deque (which allows reverse pathing as well), ships and bullets in the ArrayList, and we used the HashMap for collision to ensure a O(1) lookup time. We learned how to optimize the server enough that both the client and server could run on one machine (even a laptop) smoothly. Since the data was sent as a string, the packet size was not too much of a problem but we learned a lot about networking and multi-threading as you can imagine.

	
INFORMATION FOR WEB PAGE
	Game Name: Space Tunnels
	People Involved: Sid Asnani, Joshua Berenhaus, Steven Southall
	Description: Inspired by Ender's Game, fly though the field and find the tunnel of your opponent(s)! Place alarms to alert you when you are found and eliminate your opponent(s). Play with as many of your friends as you like, but be wary because as the number of players increases, the larger the playing field gets.
	More Information: Play against your friends using any server (input IP address as argument when running exe). The server code is provided for you to run.
	Features: Networked game that keeps track of bullets, player paths, and alarms. Bounded game environment that expands with number of players.

BUGS
	Game is easily exploitable using Network/frame-rate hacks.

COPYING/LICENSE
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
