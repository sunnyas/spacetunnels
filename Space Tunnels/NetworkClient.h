

#ifndef __SpaceTunnels__NetworkClient__
#define __SpaceTunnels__NetworkClient__

#include <iostream>

int our_connect(char* host);
int our_read(char *buf);
std::string our_string(char *buf);
int our_write(char *buf);


#endif /* defined(__SpaceTunnels__NetworkClient__) */
