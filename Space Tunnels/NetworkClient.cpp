

#include "NetworkClient.h"

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/make_shared.hpp>
#include <boost/ref.hpp>

using boost::asio::ip::tcp;


boost::asio::io_service io_service;
boost::shared_ptr<tcp::socket> our_socket;

int our_connect(char* host) {
    try {
        tcp::resolver resolver(io_service);
        tcp::resolver::query query (host, "13370");
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        tcp::resolver::iterator end;
        
        our_socket = boost::make_shared<tcp::socket>(boost::ref(io_service));
        //boost::asio::ip::tcp::no_delay option(true);
        //our_socket->set_option(option);
        
        boost::system::error_code error = boost::asio::error::host_not_found;
        while (error && endpoint_iterator != end)
        {
            our_socket->close();
            our_socket->connect(*endpoint_iterator++, error);
        }
        if (error)
            throw boost::system::system_error(error);
		return 0;
        
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    
    return -1;
    
}

int our_read(char *buffer) {
    size_t len = 0;
	static boost::asio::streambuf buf;
    if (our_socket->available() > 0) {
        boost::system::error_code error;
        
		len = boost::asio::read_until(*our_socket, buf, '\n');
		std::istream str(&buf);
		std::string s;
		std::getline(str, s);
        
        if (error == boost::asio::error::eof)
            return -1; // Connection closed cleanly by peer.
        else if (error)
            throw boost::system::system_error(error); // Some other error.
		for (int i=0; i<len; i++)
			buffer[i] = s[i];
    }
    return len;
}


std::string our_string(char *buf) {
    // boost::shared_ptr<char> p(buf, NULL);
    // std::string str(p.get());
    return std::string(buf);
}


int our_write(char *buf) {
    int len = 0;
    
    std::string mess = our_string(buf);
    boost::system::error_code error;
	len = our_socket->send(boost::asio::buffer(mess));
    
    return len;
}