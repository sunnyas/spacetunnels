//----------------------------------------------------------------------
// Best viewed with tabs every 2 columns
//----------------------------------------------------------------------
//	File: World.cpp - World methods
//----------------------------------------------------------------------

#include <cstdlib> // C++ standard definitions
#include <cmath> // math definitions
#include <string.h>
#include "NetworkClient.h"

// include files are in a slightly different location for MacOS
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "Vector3d.h"
#include "World.h" // World definitions

//----------------------------------------------------------------------
// World::World - initialize the world
//
// Initializes the camera position, resets various flags, and generates
// a 6x6 grid of snowmen, which are stored in a list.
//----------------------------------------------------------------------
World::World() // World constructor
{
	aspectRatio = 1; // default aspect ratio (will be changed)

	// Camera position
	//x = 0.0; y = -5.0; // initially 5 units south of origin

	// Mouse drag control
	isDraggingLeft = false; // true when dragging
	isDraggingRight = false;
	lastX = 0; // records the coordinate when dragging starts
	lastY = 0;

	// Arrow keys
	UpDown = 0; // nothing pressed
	LeftRight = 0;
	space = false;

	ship = new Ship(Vector3d(2, 2, 2, 0));

	players = 2;

	theta = 0; // angle for third person camera
	phi = 22;

	// set camera mode
	mode = 2;

	//the game isn't over, we just started!
	gameOver = false;
	winYet = 0;
	for (int i=0; i<10; i++)
		otherPathLengths[i] = 0;
}

//----------------------------------------------------------------------
// World::setUpCamera - set camera position and projection
//
// Places the camera at (x,y,1) pointing along (lx, ly, 0). The camera
// has a 45 degree vertical field of view. Its aspect ratio matches the
// graphics window. The near and far clipping planes are set to 1 and
// 100, respectively. (Anything closer or farther is not drawn.)
//----------------------------------------------------------------------
void World::setUpCamera() 
{
	int radius = BOX_SIZE*(players+1);
	gluLookAt(
		radius*cos(phi/180*3.14)*cos(theta/180*3.14),	radius*cos(phi/180*3.14)*sin(theta/180*3.14),	radius*sin(phi/180*3.14),
		0.0,	0.0,	0.0,
		0.0,	0.0,	1.0);

	glMatrixMode(GL_PROJECTION); // projection matrix is active
	glLoadIdentity(); // reset the projection
	gluPerspective(45.0, aspectRatio, 0.1, 1000.0); // perspective transformation
	glMatrixMode(GL_MODELVIEW); // return to modelview mode
}

//----------------------------------------------------------------------
// World::setAspectRatio - set window's aspect ratio
//----------------------------------------------------------------------
void World::setAspectRatio(float ratio) 
{
	aspectRatio = ratio;
}

//----------------------------------------------------------------------
// World::setCenter - set the coordinates of the center of the screen
//----------------------------------------------------------------------
void World::setCenter(int ww, int hh)
{
	centerX = ww;
	centerY = hh;
}

//----------------------------------------------------------------------
// World::update - update the world after each idle event
//
// Moves the camera (when up/down arrow key held) forward or backward by
// the amount saved in deltaMove.
//----------------------------------------------------------------------
void World::update() // update (with each idle event)
{
	char buf[1025];
	char command[1025];
	char *info;
	memset(buf, 0, 1025);
	memset(command, 0, 1025);

	for (int i=0; i<10; i++) {
		if (otherPathLengths[i])
			otherPathLengths[i] -= 1;
	}

	int len = our_read(buf);
	while (len > 0) {
		int i=0, lastI;
		for (; buf[i] && buf[i] != '\n' && i < 1024; i++);
		buf[i] = '\0';
		i++;
		memcpy(command, buf, i);
		while (command[0] != '\0') {
			if ((info = strstr(command, "NUMPLAYERS:")) != 0) {
				info += 11;
				players = *info - '0';
				if (players = 1)
					winYet = 1;
			}
			else if ((info = strstr(command, "DEATH")) != 0)
				winYet = 2;
			else if ((info = strstr(command, "LOCATION:")) != 0) {
				info += 9;
				char *temp;
				int x, y, z;
				temp = strtok(info, ","); x = atof(temp);
				temp = strtok(NULL, ","); y = atof(temp);
				temp = strtok(NULL, ","); z = atof(temp);
				ship->setPos(Vector3d(x, y, z, 0));
			}
			else if ((info = strstr(command, "ALARM:")) != 0) {
				info += 6;
				char *temp;
				int player;
				double x, y, z;
				temp = strtok(info, ":");
				player = atoi(temp);
				while ((temp = strtok(NULL, ",")) != NULL) {
					x = atof(temp);
					temp = strtok(NULL, ","); y = atof(temp);
					temp = strtok(NULL, ":"); z = atof(temp);
					otherPaths[player][otherPathLengths[player]] = Vector3d(x, y, z, 0);
					otherPathLengths[player]++;
				}
			}
			lastI = i;
			for (; buf[i] && buf[i] != '\n' && i < 1024; i++);
			memset(command, 0, 1025);
			if (i>lastI) {
				buf[i] = '\0';
				i++;
				memcpy(command, buf+lastI, i-lastI);
			}
		}
		memset(buf, 0, 1025);
		len = our_read(buf);
	}
	if (winYet != 2) {
		ship->update(UpDown==1, UpDown==-1, LeftRight==1, LeftRight==-1, space, e);
	}
	else
		ship->sendPos();
	space = false;
	e = false;
	glutPostRedisplay(); // redisplay everything
}

//----------------------------------------------------------------------
// World::draw - draw everything
//
// Moves the camera (when up/down arrow key held) forward or backward by
// the amount saved in deltaMove.
//----------------------------------------------------------------------
void World::draw()
{
	setUpCamera(); // set up the camera

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(1.0, 0.0, 0.0, 0.5);
	glutWireCube(BOX_SIZE*players); //set up the wire cube battle area

	//check to make sure they haven't left the cube, if they have, make the side red
	gameOver = OutOfBounds::checkCube(ship);
	if(gameOver && winYet != 2){ //if the game is over at this point, it was because we went out of bounds
		char buf[1024];
		sprintf(buf, "DEATH\n");
		our_write(buf);
		winYet = 2;
	}
	//TODO: Check to see if the other player has conceeded to check win condition
	switch(winYet){
		case 1: 
			OutOfBounds::writeWords("You Win", .5, .5); break;
		case 2: 
			OutOfBounds::writeWords("You Lose", .5, .5); break;
		default:
		case 0:
			break;
	}
	for (int i=0; i<10; i++) {
		if (otherPathLengths[i] == 0)
			continue;
		//Draw the trail
		Vector3d temp, temp2;
		Cylinder *c;
		for (int j=0; j<otherPathLengths[i]; j++)
		{
			temp = otherPaths[i][j%TRAIL_LENGTH] - otherPaths[i][(j+1)%TRAIL_LENGTH];
			if (temp.length() < 0.1)
			{
				float col[3] = {0.8, 0.8, 0.8};
				c = new Cylinder(otherPaths[i][j%TRAIL_LENGTH] + temp*0.3, otherPaths[i][(j+1)%TRAIL_LENGTH] - temp*0.3, col);
				c->draw();
				delete c;
			}
		}
	}
	ship->draw();
	glDisable(GL_BLEND);
}



//----------------------------------------------------------------------
// Process keyboard events
// 
// When one of the arrow keys is first depressed, we set deltaMove.
// With each subsequent idle event, we move the camera by this amount
// based on the direction it is currently pointing.
//----------------------------------------------------------------------
void World::processNormalKeys(unsigned char key, int xx, int yy)
{
	const int ESC = 27; // escape key (for exit)

	if (key == ESC || key == 'q' || key == 'Q') {
		char buf[1024];
		sprintf(buf, "DISCONNECT\n");
		our_write(buf);
		exit(0);
	}
	switch (key)
	{
	case 'W' :
	case 'w' : UpDown = 1; break;
	case 'S' : 
	case 's' : UpDown = -1; break;
	case 'A' : 
	case 'a' : LeftRight = 1; break;
	case 'D' : 
	case 'd' : LeftRight = -1; break;
	case 'e' : e = true; break;
	case ' ' : space = true; break;
	default: break;
	}
} 

void World::releaseNormalKeys(unsigned char key, int xx, int yy)
{
	switch (key)
	{
	case 'W' :
	case 'w' :
	case 'S' :
	case 's' : UpDown = 0; break;
	case 'A' :
	case 'a' :
	case 'D' :
	case 'd' : LeftRight = 0; break;
	default: break;
	}
}

void World::pressSpecialKey(int key, int xx, int yy)
{
	switch (key) {
		case GLUT_KEY_UP : UpDown = 1; break;
		case GLUT_KEY_DOWN : UpDown = -1; break;
		case GLUT_KEY_LEFT : LeftRight = 1; break;
		case GLUT_KEY_RIGHT : LeftRight = -1; break;
	}
} 

void World::releaseSpecialKey(int key, int xx, int yy) 
{
	switch (key) {
		case GLUT_KEY_UP :
		case GLUT_KEY_DOWN : UpDown = 0; break;
		case GLUT_KEY_LEFT :
		case GLUT_KEY_RIGHT : LeftRight = 0; break;
	}
} 

//----------------------------------------------------------------------
// Process mouse drag events
// 
// This is called when dragging motion occurs. The variable
// angle stores the camera angle at the instance when dragging
// started, and deltaAngle is a additional angle based on the
// mouse movement since dragging started.
//----------------------------------------------------------------------
void World::mouseMove(int xx, int yy) 
{ 	
	if (isDraggingLeft && mode == 1) { // only when dragging in mode 1
		//y += (xx - lastX)*0.03;
		//x += (yy - lastY)*0.03;
		lastX = xx;
		lastY = yy;
	}
	if (isDraggingLeft && mode == 2) { // only when dragging in mode 2
		theta -= (xx - lastX);
		phi += (yy - lastY);
		if (phi > 175)
			phi = 175;
		if (phi < -175)
			phi = -175;	
		lastX = xx;
		lastY = yy;
	}
	if ((isDraggingLeft || isDraggingRight) && mode == 0 && xx != centerX && yy != centerY) { // always in mode 3
		theta -= (xx - centerX);
		phi -= (yy - centerY);
		glutWarpPointer(centerX, centerY);
	}
}

void World::mouseButton(int button, int state, int xx, int yy) 
{
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) { // left mouse button pressed
			isDraggingLeft = true; // start dragging
			lastX = xx; // save coords where button first pressed
			lastY = yy;
		}
		else  { /* (state = GLUT_UP) */
			isDraggingLeft = false; // no longer dragging
		}
	}
	else if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN) { // right mouse button pressed
			isDraggingRight = true;
			lastX = xx;
			lastY = yy;
		}
		else { /* (state = GLUT_UP) */
			isDraggingRight = false; // no longer dragging
		}
	}
}

void World::mousePassive(int xx, int yy) {
	if (mode == 0 && xx != centerX && yy != centerY) {
		theta -= (xx - centerX);
		phi -= (yy - centerY);
		glutWarpPointer(centerX, centerY);
	}
}