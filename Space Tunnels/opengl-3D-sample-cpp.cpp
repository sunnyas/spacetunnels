//----------------------------------------------------------------------
// Best viewed with tabs every 2 columns
//----------------------------------------------------------------------
//	File: OpenGL-3D-Sample.cpp - Sample 3D OpenGL/GLUT program
//	Author: Dave Mount (adapted from a demo on Lighthouse3D)
//	For: CMSC 425
//
//	This is a sample program that illustrates OpenGL and GLUT. It
//	renders a picture of 36 snowmen. The camera can be moved by dragging
//	the mouse. The camera moves forward by hitting the up-arrow key and
//	back by moving the down-arrow key. Hit ESC or 'q' to exit.
//
//	Warning #1: This program uses the function glutSpecialUpFunc, which
//	may not be available in all GLUT implementations. If your system
//	does not have it, you can comment this line out, but the up arrow
//	processing will not be correct.
//
//	Warning #2: This is a minimalist program. Very little attention has
//	been paid to good programming technique.
//----------------------------------------------------------------------

#include <cstdlib> // C++ standard definitions
#include <iostream> // I/O stream

#include "NetworkClient.h"

#include "World.h" // world object definitions

// include files are in a slightly different location for MacOS
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//----------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------

World theWorld; // the world

//----------------------------------------------------------------------
// Reshape callback
//
// Window size has been set/changed to w by h pixels.
//----------------------------------------------------------------------
void changeSize(int w, int h) 
{
	theWorld.setAspectRatio(float(w)/float(h)); // set new aspect ratio
	theWorld.setCenter(w/2, h/2);
	glViewport(0, 0, w, h); // set viewport (drawing area) to entire window
}

//----------------------------------------------------------------------
// Update with each idle event
//
// This incrementally moves the camera and requests that the scene be
// redrawn.
//----------------------------------------------------------------------
void update(int) 
{
	theWorld.update(); // update the World
	glutPostRedisplay(); // redraw everything (since camera may move)
	glutTimerFunc(33, update, 0);
}

//----------------------------------------------------------------------
// Draw the entire scene
//
// We clear the buffers using light blue (so the sky is already drawn)
// and then draw the world.
//----------------------------------------------------------------------
void draw() 
{
	glClearColor(0.0, 0.0, 0.0, 1.0); // background (sky) color is light blue
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear buffers
	glLoadIdentity(); // reset modelview transformation

	theWorld.draw(); // draw the world

	glutSwapBuffers(); // make it all visible
} 

//----------------------------------------------------------------------
// User-input callbacks
//
// C++ does not allow callbacks to be made to class methods, so 
// these are just redirections to the corresponding World handler.
//----------------------------------------------------------------------
void processNormalKeys(unsigned char key, int xx, int yy) { theWorld.processNormalKeys(key, xx, yy); }
void releaseNormalKeys(unsigned char key, int xx, int yy) { theWorld.releaseNormalKeys(key, xx, yy); }
void pressSpecialKey(int key, int xx, int yy) { theWorld.pressSpecialKey(key, xx, yy); }
void releaseSpecialKey(int key, int xx, int yy)  { theWorld.releaseSpecialKey(key, xx, yy); }
void mouseMove(int xx, int yy)  { theWorld.mouseMove(xx, yy); }
void mouseButton(int button, int state, int xx, int yy)  { theWorld.mouseButton(button, state, xx, yy); }
void mousePassive(int xx, int yy) { theWorld.mousePassive(xx, yy); }

//----------------------------------------------------------------------
// Main program  - standard GLUT initializations and callbacks
//----------------------------------------------------------------------
int main(int argc, char **argv) 
{
	if (argc < 2) {
		std::cout << "\n\
					 must be run with a host address as an argument \n";
		return 0;
	}

	std::cout << "\n\
-----------------------------------------------------------------------\n\
  OpenGL Sample Program:\n\
  - Drag mouse left-right to rotate camera\n\
  - Hold up-arrow/down-arrow to move camera forward/backward\n\
  - q or ESC to quit\n\
-----------------------------------------------------------------------\n";

	// general initializations
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(800, 500);
	glutCreateWindow("OpenGL/GLUT Sample Program");

	// register callbacks
	glutReshapeFunc(changeSize); // window reshape callback
	glutDisplayFunc(draw); // (re)display callback
	glutTimerFunc(33, update, 0); // incremental update 
	glutIgnoreKeyRepeat(1); // ignore key repeat when holding key down
	glutMouseFunc(mouseButton); // process mouse button push/release
	glutMotionFunc(mouseMove); // process mouse dragging motion
	glutPassiveMotionFunc(mousePassive); // process mouse passive motion
	glutKeyboardFunc(processNormalKeys); // process standard key clicks
	glutKeyboardUpFunc(releaseNormalKeys); // process standard key release
	glutSpecialFunc(pressSpecialKey); // process special key pressed
						// Warning: Nonstandard function! Delete if desired.
	glutSpecialUpFunc(releaseSpecialKey); // process special key release

	// OpenGL init
	glEnable(GL_DEPTH_TEST);

	if (our_connect(argv[1]) < 0)
		return -1;

	char buf[1024];
	sprintf(buf, "CONNECT\n");
	our_write(buf);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 0; // this is just to keep the compiler happy
}