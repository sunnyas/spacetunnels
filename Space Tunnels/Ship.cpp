#include "NetworkClient.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "Ship.h"
#include "Cylinder.h"

Ship::Ship(Vector3d initPos)
{
	pos = initPos;
	dir = Vector3d(1, 0, 3.14/2, 0);
	Vector3d temp = Vector3d::sphericalToCartesian(dir);
	perp = Vector3d::cross(Vector3d::sphericalToCartesian(dir), Vector3d::zUnit());
	for (int i=0; i<TRAIL_LENGTH; i++)
		path[i] = initPos;
	for (int i=0; i<MAX_ALERTS; i++)
		alerts[i] = Vector3d(10000, 10000, 10000, 0);
}

void Ship::draw()
{
	float angle = -dir[2]*180/3.14;

	while (angle < -180)
		angle += 360;
	while (angle > 180)
		angle -= 360;
	if (angle > 0)
		angle = -angle;
	//Draw the ship
	glPushMatrix();
		glColor3f(0.0, 0.0, 1.0);
		glTranslated(pos[0], pos[1], pos[2]);
		glRotated(angle, perp[0], perp[1], perp[2]);
		GLUquadric *quad = gluNewQuadric();
		gluCylinder(quad, 0.5, 0, 1, 20, 20);
	glPopMatrix();
	
	//Draw the trail
	Vector3d temp, temp2;
	Cylinder *c;
	for (int i=0; i<TRAIL_LENGTH; i++)
	{
		temp = path[i%TRAIL_LENGTH] - path[(i+1)%TRAIL_LENGTH];
		if (temp.length() < 0.1)
		{
			float col[3] = {0.8, 0.8, 0.8};
			for (int j=0; j<MAX_ALERTS; j++)
			{
				temp2  = path[i%TRAIL_LENGTH] - alerts[j];
				if (temp2.length() < 0.2)
				{
					col[0] = 1.0;
					col[1] = 0.0;
					col[2] = 0.0;
				}
			}
			c = new Cylinder(path[i%TRAIL_LENGTH] + temp*0.3, path[(i+1)%TRAIL_LENGTH] - temp*0.3, col);
			c->draw();
			delete c;
		}
	}
}

static int current = 0;

void Ship::update(bool up, bool down, bool left, bool right, bool space, bool e)
{
	static int i = 0;

	if (up && !down)
		dir[2] -= 0.03;
	else if (down && !up)
		dir[2] += 0.03;

	if (left && !right)
		dir[1] += 0.03;
	else if (right && !left)
		dir[1] -= 0.03;

	Vector3d temp = dir;

	while (temp[2] < -180)
		temp[2] += 2*3.14;
	while (temp[2] > 180)
		temp[2] -= 2*3.14;
	if (temp[2] < 0) {
		temp[2] = -temp[2];
		temp[1] += 3.14;
	}

	perp = Vector3d::cross(Vector3d::sphericalToCartesian(temp), Vector3d::zUnit());

	temp = Vector3d::sphericalToCartesian(temp);
	pos += temp*0.05;
	path[current%TRAIL_LENGTH] = pos;
	current++;

	char buf[1024];
	sprintf(buf, "LOCATION:%f,%f,%f\n", pos[0], pos[1], pos[2]);
	our_write(buf);
	
	if (e) 
	{
		alerts[i%MAX_ALERTS] = pos;
		i++;
		sprintf(buf, "ALARM:%f,%f,%f\n", pos[0], pos[1], pos[2]);
		our_write(buf);
	}

	if (space)
	{
		sprintf(buf, "FIRE:%f,%f,%f:%f,%f,%f\n", pos[0], pos[1], pos[2], temp[0], temp[1], temp[2]);
		our_write(buf);
	}
}
Vector3d Ship::getPos(){
	return pos;
}
void Ship::setPos(Vector3d p) {
	pos = p;
	for (int i=0; i<TRAIL_LENGTH; i++)
		path[i] = p;
}
void Ship::sendPos() {
	path[current%TRAIL_LENGTH] = pos;
	current++;

	char buf[1024];
	sprintf(buf, "LOCATION:%f,%f,%f\n",pos[0], pos[1], pos[2]);
	our_write(buf);
}