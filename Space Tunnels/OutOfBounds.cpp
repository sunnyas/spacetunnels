#include "OutOfBounds.h"
#include <time.h>
#include <ctime>
#include <sys/timeb.h>
/*!
* Checks to make sure they haven't left the cube
* Makes a side of the cube red to indicate that they've left
**/
timeb currTime;
unsigned short pTime;
float const static OUT_OF_BOUNDS_LIMIT = 10;
float const static TRANSLUCENCY = 0.1;
float const static POSITION_OF_WARNING_TEXT = 0.02;

	// set the OutOfBounds time
	static bool inBounds = true;
	static bool newOOB = true; //checks if the person is in bounds and if it is a new Out of Bounds
	float elapsedTime = 0;
	
bool OutOfBounds::checkCube(Ship* ship){
	//if they hit the bottom
	if(ship->getPos().getZ() <= -BOX_SIZE){
		drawFace(Point3d(-BOX_SIZE, -BOX_SIZE, -BOX_SIZE, 1), Point3d(BOX_SIZE, BOX_SIZE, -BOX_SIZE, 1), TRANSLUCENCY);
	}
	//if they hit the top
	else if(ship->getPos().getZ() >= BOX_SIZE){
		drawFace(Point3d(BOX_SIZE, BOX_SIZE, BOX_SIZE, 1), Point3d(-BOX_SIZE, -BOX_SIZE, BOX_SIZE, 1), TRANSLUCENCY);
	}
	//if they hit the left
	else if(ship->getPos().getY() <= -BOX_SIZE){
		drawFace(Point3d(-BOX_SIZE, -BOX_SIZE, -BOX_SIZE, 1), Point3d(BOX_SIZE, -BOX_SIZE, BOX_SIZE, 1), TRANSLUCENCY);
	}
	//if they hit the right
	else if(ship->getPos().getY() >= BOX_SIZE){
		drawFace(Point3d(BOX_SIZE, BOX_SIZE, BOX_SIZE, 1), Point3d(-BOX_SIZE, BOX_SIZE, -BOX_SIZE, 1), TRANSLUCENCY);
	}
	//if they hit the front
	else if(ship->getPos().getX() >= BOX_SIZE){
		drawFace(Point3d(BOX_SIZE, BOX_SIZE, BOX_SIZE, 1), Point3d(BOX_SIZE, -BOX_SIZE, -BOX_SIZE, 1), TRANSLUCENCY);
	}
	//if they hit the back
	else if(ship->getPos().getX() <= -BOX_SIZE){
		drawFace(Point3d(-BOX_SIZE, -BOX_SIZE, -BOX_SIZE, 1), Point3d(-BOX_SIZE, BOX_SIZE, BOX_SIZE, 1), TRANSLUCENCY);	
	} else { //we're in bounds
		inBounds = true;
		newOOB = true;
		elapsedTime = 0;
		return false;
	}
	inBounds = false;
	return showOutOfBoundsWarning();
	
}

/**
	Creates a warning and displays it on the page
*/
bool OutOfBounds::showOutOfBoundsWarning(){
	if(newOOB){ //if they newly went out of bounds, we need to know this so that we can grab a new time before we set the current time
		ftime(&currTime); //grab a new time
		newOOB = false; //it's no longer new
	}
	pTime = currTime.millitm; //set current time
	ftime(&currTime); //grab a new time for when we check again later
	if(currTime.millitm < pTime){
		elapsedTime = elapsedTime + 1.0f;
	}
	elapsedTime = elapsedTime + (currTime.millitm - pTime)/1000.0; //calculate new elapsed time
	std::string result; //final string
	float timeLeft = OUT_OF_BOUNDS_LIMIT - elapsedTime; //calculate how much time is left
	char numstr[21];
	if(timeLeft >= 0){
		sprintf(numstr, "%.2f", timeLeft); //put the time left into a variable with 2 decimal points
		std::string a = std::string("Out of Bounds, you have ");
		std::string b = std::string(" seconds remaining to return");
		a.append(std::string(numstr)).append(b); //concat everything

		writeWords(a, POSITION_OF_WARNING_TEXT, POSITION_OF_WARNING_TEXT); //send it to the pretty function
		return false;
	} else { //otherwise if the game is over
		return true;
	}
}

/**
	Writes words at a certain position
*/
void OutOfBounds::writeWords(std::string words, float posx, float posy){

	//Don't worry about why this works. It only kind of makes sense
	glMatrixMode(GL_PROJECTION); //go to projection mode             
	glPushMatrix();
		glLoadIdentity(); //load our new matrix
		gluOrtho2D(0.0, 1.0, 0.0, 1.0); // map unit square to viewport
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
			glLoadIdentity(); //load our newer matrix
			glRasterPos2f(posx, posy+0.01);
			for(int i = 0; i < ((int)words.length()); i++){
				glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, words[i]);
			}
			
		glPopMatrix();
		glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
}

//----------------------------------------------------------------------
// World::drawFace - draw the given axis-aligned face of a cube with
//		solid edges and alpha given in the last argument
//----------------------------------------------------------------------
void OutOfBounds::drawFace(Point3d p1, Point3d p2, float alpha)
{
	glColor3f(1.0, 1.0, 1.0);
	if (p1[0] == p2[0])
	{
		glBegin(GL_LINE_STRIP);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p2[1], p1[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p1[1], p2[2]);
			glVertex3f(p1[0], p1[1], p1[2]);
		glEnd();
	}
	else if (p1[1] == p2[1])
	{
		glBegin(GL_LINE_STRIP);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p1[1], p2[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p2[1], p1[2]);
			glVertex3f(p1[0], p1[1], p1[2]);
		glEnd();
	}
	else /* (p1[2] == p2[2]) */
	{
		glBegin(GL_LINE_STRIP);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p2[1], p1[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p1[1], p2[2]);
			glVertex3f(p1[0], p1[1], p1[2]);
		glEnd();
	}

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(1.0, 0.0, 0.0, alpha);
	if (p1[0] == p2[0])
	{
		glBegin(GL_QUADS);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p2[1], p1[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p1[1], p2[2]);
		glEnd();
	}
	else if (p1[1] == p2[1])
	{
		glBegin(GL_QUADS);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p1[1], p2[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p2[1], p1[2]);
		glEnd();
	}
	else /* (p1[2] == p2[2]) */
	{
		glBegin(GL_QUADS);
			glVertex3f(p1[0], p1[1], p1[2]);
			glVertex3f(p1[0], p2[1], p1[2]);
			glVertex3f(p2[0], p2[1], p2[2]);
			glVertex3f(p2[0], p1[1], p2[2]);
		glEnd();
	}
	glDisable(GL_BLEND);
}