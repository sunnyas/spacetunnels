#ifndef SHIP_H
#define SHIP_H

#include "Vector3d.h"

#define TRAIL_LENGTH 1000
#define MAX_ALERTS 3

class Ship
{
private:
	Vector3d pos, dir; // dir is (r, theta, phi)
	Vector3d perp;
	double angle;
	Vector3d path[TRAIL_LENGTH];
	Vector3d alerts[MAX_ALERTS];

public:
	Ship(Vector3d initPos);
	void draw();
	void update(bool up, bool down, bool left, bool right, bool space, bool e);
	Vector3d getPos();
	void setPos(Vector3d p);
	void sendPos();
};

#endif

