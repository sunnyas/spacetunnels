//----------------------------------------------------------------------
// Best viewed with tabs every 2 columns
//----------------------------------------------------------------------
//	File: World.h - Definitions for World object
//----------------------------------------------------------------------
#ifndef WORLD_H
#define WORLD_H

#include <list> // STL list
#include "Cylinder.h"
#include "Ship.h"
#include "OutOfBounds.h"
#define BOX_SIZE 25

//----------------------------------------------------------------------
// World
//
// Our world is rather simple. It consists of a ground, a collection of
// snowmen, and a camera. We provide methods for constructing the world,
// for drawing the world, and updating the world. The update procedure
// involves moving the camera as long as the user is holding down either
// the up or down arrow keys.
//
// The coordinate system is set up so that the (x,y)-coordinate plane
// is the ground, and the z-axis is directed upwards. The y-axis points
// to the north and the x-axis points to the east.
//
// The values (x,y) are the current camera position. The values (lx, ly)
// point in the direction the camera is looking. The variables angle and
// deltaAngle control the camera's angle. The variable deltaMove
// indicates the amount of incremental motion for the camera with each
// redraw cycle. The variables isDragging and xDragStart are used to
// monitor the mouse when it drags (with the left button down).
//----------------------------------------------------------------------
class World {
private: // private data
	
	float aspectRatio; // aspect ratio of graphics window
	int centerX, centerY; // coords for the center of the screen
	//float x, y; // camera position
	int UpDown; // Arrow keys
	int LeftRight;
	bool space;
	bool e;

	float theta, phi; // angle of rotation for the camera direction

	bool isDraggingLeft, isDraggingRight; // true when mouse is dragging
	int lastX, lastY; // records the coordinate when dragging starts

	Ship *ship;

	int players;

	int mode;
	bool gameOver; //is the game over? (true if game over, false otherwise)
	int winYet; //0 is neither lose nor win, 1 is win, 2 is lose

private: // private utility functions
	void setUpCamera(); // camera set-up utility
	Vector3d otherPaths[10][TRAIL_LENGTH];
	int otherPathLengths[10];
	int numPaths;
	
	
	
public: // public interface
	World(); // constructor
	void setAspectRatio(float ratio); // set the aspect ratio
	void setCenter(int ww, int hh); // set the center of the screen
	void update(); // update (with each idle event)
	void draw(); // draw everything
	void processNormalKeys(unsigned char key, int xx, int yy);
	void releaseNormalKeys(unsigned char key, int xx, int yy);
	void pressSpecialKey(int key, int xx, int yy); // special key press
	void releaseSpecialKey(int key, int xx, int yy); // special key release
	void mouseMove(int xx, int yy); // mouse dragged
	void mouseButton(int button, int state, int xx, int yy); // mouse button
	void mousePassive(int xx, int yy); // mouse moved
	
	
	
};

#endif